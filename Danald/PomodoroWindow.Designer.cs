﻿namespace HostsSwitcher
{
    partial class PomodoroWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PomodoroWindow));
            this.HideShowWindowButton = new System.Windows.Forms.Button();
            this.TimeLeftLabel = new System.Windows.Forms.Label();
            this.StartButton = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.WorkInfoLabel = new System.Windows.Forms.Label();
            this.BreakInfoLabel = new System.Windows.Forms.Label();
            this.StopResumeButton = new System.Windows.Forms.Button();
            this.ResetButton = new System.Windows.Forms.Button();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // HideShowWindowButton
            // 
            this.HideShowWindowButton.Location = new System.Drawing.Point(143, 0);
            this.HideShowWindowButton.Margin = new System.Windows.Forms.Padding(0);
            this.HideShowWindowButton.Name = "HideShowWindowButton";
            this.HideShowWindowButton.Size = new System.Drawing.Size(26, 26);
            this.HideShowWindowButton.TabIndex = 0;
            this.HideShowWindowButton.Text = ">";
            this.HideShowWindowButton.UseVisualStyleBackColor = true;
            this.HideShowWindowButton.Click += new System.EventHandler(this.HideShowWindowButton_Click);
            // 
            // TimeLeftLabel
            // 
            this.TimeLeftLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TimeLeftLabel.AutoSize = true;
            this.TimeLeftLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.TimeLeftLabel.Location = new System.Drawing.Point(55, 0);
            this.TimeLeftLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.TimeLeftLabel.Name = "TimeLeftLabel";
            this.TimeLeftLabel.Size = new System.Drawing.Size(86, 31);
            this.TimeLeftLabel.TabIndex = 1;
            this.TimeLeftLabel.Text = "label1";
            // 
            // StartButton
            // 
            this.StartButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.StartButton.Location = new System.Drawing.Point(0, 42);
            this.StartButton.Margin = new System.Windows.Forms.Padding(2);
            this.StartButton.Name = "StartButton";
            this.StartButton.Size = new System.Drawing.Size(170, 22);
            this.StartButton.TabIndex = 2;
            this.StartButton.Text = "START";
            this.StartButton.UseVisualStyleBackColor = true;
            this.StartButton.Click += new System.EventHandler(this.StartButton_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.WorkInfoLabel);
            this.flowLayoutPanel1.Controls.Add(this.BreakInfoLabel);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(2);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(60, 42);
            this.flowLayoutPanel1.TabIndex = 3;
            // 
            // WorkInfoLabel
            // 
            this.WorkInfoLabel.AutoSize = true;
            this.WorkInfoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.WorkInfoLabel.Location = new System.Drawing.Point(2, 0);
            this.WorkInfoLabel.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.WorkInfoLabel.Name = "WorkInfoLabel";
            this.WorkInfoLabel.Size = new System.Drawing.Size(41, 15);
            this.WorkInfoLabel.TabIndex = 0;
            this.WorkInfoLabel.Text = "label1";
            // 
            // BreakInfoLabel
            // 
            this.BreakInfoLabel.AutoSize = true;
            this.BreakInfoLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.BreakInfoLabel.Location = new System.Drawing.Point(2, 18);
            this.BreakInfoLabel.Margin = new System.Windows.Forms.Padding(2, 3, 2, 0);
            this.BreakInfoLabel.Name = "BreakInfoLabel";
            this.BreakInfoLabel.Size = new System.Drawing.Size(41, 15);
            this.BreakInfoLabel.TabIndex = 1;
            this.BreakInfoLabel.Text = "label2";
            // 
            // StopResumeButton
            // 
            this.StopResumeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.StopResumeButton.Location = new System.Drawing.Point(0, 42);
            this.StopResumeButton.Margin = new System.Windows.Forms.Padding(2);
            this.StopResumeButton.Name = "StopResumeButton";
            this.StopResumeButton.Size = new System.Drawing.Size(105, 22);
            this.StopResumeButton.TabIndex = 4;
            this.StopResumeButton.Text = "STOP";
            this.StopResumeButton.UseVisualStyleBackColor = true;
            this.StopResumeButton.Click += new System.EventHandler(this.StopResumeButton_Click);
            // 
            // ResetButton
            // 
            this.ResetButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ResetButton.Location = new System.Drawing.Point(117, 42);
            this.ResetButton.Margin = new System.Windows.Forms.Padding(2);
            this.ResetButton.Name = "ResetButton";
            this.ResetButton.Size = new System.Drawing.Size(52, 22);
            this.ResetButton.TabIndex = 5;
            this.ResetButton.Text = "RESET";
            this.ResetButton.UseVisualStyleBackColor = true;
            this.ResetButton.Click += new System.EventHandler(this.ResetButton_Click);
            // 
            // PomodoroWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(170, 64);
            this.ControlBox = false;
            this.Controls.Add(this.ResetButton);
            this.Controls.Add(this.StopResumeButton);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.StartButton);
            this.Controls.Add(this.TimeLeftLabel);
            this.Controls.Add(this.HideShowWindowButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PomodoroWindow";
            this.RightToLeftLayout = true;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.TopMost = true;
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button HideShowWindowButton;
        private System.Windows.Forms.Label TimeLeftLabel;
        private System.Windows.Forms.Button StartButton;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label WorkInfoLabel;
        private System.Windows.Forms.Label BreakInfoLabel;
        private System.Windows.Forms.Button StopResumeButton;
        private System.Windows.Forms.Button ResetButton;
    }
}