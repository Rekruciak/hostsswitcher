﻿using Danald.Properties;
using HostsSwitcher;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Danald.AppContext
{
    public class PomodoroAppContext
    {
        public WindowMode _currentWindowMode = WindowMode.NotSet;
        public WindowMode CurrentWindowMode
        {
            get => _currentWindowMode;
            set
            {
                if (_currentWindowMode != value)
                {
                    _currentWindowMode = value;
                    PomodoroWindow.CaptureWindowModeChange_ThreadSafe();
                    FinishWorkService.Instance.LogPomodoroWindowModeChanged(value);
                }
            }
        }

        public enum WindowMode
        {
            NotSet,
            ReadyToStart,
            Working,
            Resting,
            StoppedWorking,
            StoppedResting,
            Finished,
        }

        Timer SecondElapsedTimer = new Timer()
        {
            Interval = 1000,
        };

        public string TimeLeft_UserFriendly
        {
            get
            {
                if (SecondsLeft <= 0)
                    return "00:00";

                int minutes = (int)Math.Floor(SecondsLeft / 60d);
                int secounds = SecondsLeft - (60 * minutes);
                return $"{minutes.ToString("00")}:{secounds.ToString("00")}";
            }
        }

        public int SecondsLeft;

        public int WorkTime = Convert.ToInt32(
            ConfigurationManager.AppSettings.Get("WorkTime") ?? "45");

        public int WorkTimeInSeconds => WorkTime * 60;

        public int BreakTime = Convert.ToInt32(
            ConfigurationManager.AppSettings.Get("BreakTime") ?? "5");

        public int BreakTimeInSeconds => BreakTime * 60;

        public int WorkTimesInRow => Convert.ToInt32(
            ConfigurationManager.AppSettings.Get("WorkTimesInRow") ?? "2");

        public int BreakTimesInRow => WorkTimesInRow - 1;

        public int FinishedWorkLp = 0;
        public int FinishedBreakLp = 0;

        public string CurrentWork_UserFriendly =>
            $"{FinishedWorkLp}{((CurrentWindowMode == WindowMode.Working || CurrentWindowMode == WindowMode.StoppedWorking) ? $"+" : "")}/{WorkTimesInRow}";

        public string CurrentBreak_UserFiendly =>
            $"{FinishedBreakLp}{((CurrentWindowMode == WindowMode.Resting || CurrentWindowMode == WindowMode.StoppedResting) ? $"+" : "")}/{BreakTimesInRow}";

        private PomodoroWindow PomodoroWindow;

        public PomodoroAppContext()
        {
            PomodoroWindow = new PomodoroWindow(this);
            PomodoroWindow.FormClosed += PomodoroWindow_FormClosed;

            ResetEnvironment();
            CurrentWindowMode = WindowMode.ReadyToStart;

            SecondElapsedTimer.Interval = 1000;
            SecondElapsedTimer.Elapsed += SecoundElapsedTimer_Elapsed;
        }

        private void SecoundElapsedTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (SecondsLeft == 0)
            {
                SecondElapsedTimer.Stop();
                TimerFinished();
                return;
            }

            SecondsLeft--;
            PomodoroWindow.UpdateTime();
        }

        public void TimerFinished()
        {
            SoundsPlayer.PlayPokerChipsSound();

            switch (CurrentWindowMode)
            {
                case WindowMode.Working: // Start resting or finish
                    FinishedWorkLp++;
                    if (FinishedWorkLp == WorkTimesInRow) // Finish pomodoro
                    {
                        CurrentWindowMode = WindowMode.Finished;
                        FinishWorkService.Instance.SaveFinishedWork();
                    }
                    else
                    {
                        SecondsLeft = BreakTimeInSeconds; // Start resting
                        SecondElapsedTimer.Start();
                        CurrentWindowMode = WindowMode.Resting;
                    }
                    break;
                case WindowMode.Resting: // Start next working time
                    FinishedBreakLp++;
                    SecondsLeft = WorkTimeInSeconds;
                    SecondElapsedTimer.Start();
                    CurrentWindowMode = WindowMode.Working;
                    break;
            }
        }

        public void ResetEnvironment()
        {
            SecondElapsedTimer.Stop();
            FinishedWorkLp = 0;
            FinishedBreakLp = 0;
            SecondsLeft = WorkTimeInSeconds;
            CurrentWindowMode = WindowMode.ReadyToStart;
        }

        public void ResetClicked() // User clicks reset button
        {
            FinishWorkService.Instance.Reset();
            ResetEnvironment();
        }

        public void Start() // User clicks start button
        {
            switch (CurrentWindowMode)
            {
                case WindowMode.ReadyToStart: //Start first working time
                    SecondElapsedTimer.Start();
                    CurrentWindowMode = WindowMode.Working;
                    break;
                case WindowMode.Finished: //Woah, next pomodoros block :D
                    FinishedWorkLp = 0;
                    FinishedBreakLp = 0;
                    SecondsLeft = WorkTimeInSeconds;
                    SecondElapsedTimer.Start();
                    CurrentWindowMode = WindowMode.Working;
                    break;
            }
        }

        public void StopResume() // User clicks stop button
        {
            switch (CurrentWindowMode)
            {
                case WindowMode.Working: //STOP
                    SecondElapsedTimer.Stop();
                    CurrentWindowMode = WindowMode.StoppedWorking;
                    break;

                case WindowMode.Resting: //STOP
                    SecondElapsedTimer.Stop();
                    CurrentWindowMode = WindowMode.StoppedResting;
                    break;

                case WindowMode.StoppedResting: //START NEW WORK_TIME
                    FinishedBreakLp++;
                    SecondsLeft = WorkTimeInSeconds;
                    CurrentWindowMode = WindowMode.Working;
                    break;

                case WindowMode.StoppedWorking: //RESUME TIMER
                    SecondElapsedTimer.Start();
                    CurrentWindowMode = WindowMode.Working;
                    break;
            }
        }


        // User closes stick window with alt+f4 which kills instance
        // (we need to have instance to show/hide)
        private void PomodoroWindow_FormClosed(object sender, System.Windows.Forms.FormClosedEventArgs e)
        {
            PomodoroWindow = new PomodoroWindow(this);
        }

        public void Pomodoro_OnClick(object sender, EventArgs e)
        {
            if (PomodoroWindow?.Visible ?? false)
                PomodoroWindow.Hide();
            else
                PomodoroWindow.Show();
        }

    }
}
