﻿using Danald;
using Danald.AppContext;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Danald.AppContext.PomodoroAppContext;
using static System.Net.Mime.MediaTypeNames;

namespace HostsSwitcher
{
    public partial class PomodoroWindow : Form
    {
        PomodoroAppContext PomodoroAppContext;

        public PomodoroWindow(PomodoroAppContext pomodoroAppContext)
        {
            InitializeComponent();
            PomodoroAppContext = pomodoroAppContext;
            Load += PomodoroWindow_Load;
        }

        public void SetButtonsVisibilityAccordingToWindowMode()
        {
            var currentWindowMode = PomodoroAppContext.CurrentWindowMode;

            if ((new List<WindowMode> {
                WindowMode.ReadyToStart,
                WindowMode.Finished})
                .Contains(currentWindowMode))
            {
                StartButton.Visible = true;
                StopResumeButton.Visible = false;
                ResetButton.Visible = false;
            }

            if ((new List<WindowMode> {
                WindowMode.Working,
                WindowMode.Resting,
                WindowMode.StoppedWorking,
                WindowMode.StoppedResting})
                .Contains(currentWindowMode))
            {
                StartButton.Visible = false;
                StopResumeButton.Visible = true;
                ResetButton.Visible = true;

                StopResumeButton.Text =
                    currentWindowMode == WindowMode.Working || currentWindowMode == WindowMode.Resting
                        ? "STOP" : "START";
            }
        }

        public void CaptureWindowModeChange()
        {
            IsWindowFullyVisible = true;
            ReloadWindowFullyVisibility();

            WorkInfoLabel.Text = $"W: {PomodoroAppContext.CurrentWork_UserFriendly}";
            BreakInfoLabel.Text = $"B: {PomodoroAppContext.CurrentBreak_UserFiendly}";
            TimeLeftLabel.Text = PomodoroAppContext.TimeLeft_UserFriendly;

            SetButtonsVisibilityAccordingToWindowMode();
        }

        public void CaptureWindowModeChange_ThreadSafe()
        {
            if (InvokeRequired)
            {
                Action safeUpdate = delegate { CaptureWindowModeChange(); };
                Invoke(safeUpdate);
            }
            else
                CaptureWindowModeChange();
        }

        private void PomodoroWindow_Load(object sender, EventArgs e)
        {
            SetWindowLocationAndSize();
        }

        bool IsWindowFullyVisible = true;

        private void HideShowWindowButton_Click(object sender, EventArgs e)
        {
            IsWindowFullyVisible = !IsWindowFullyVisible;
            ReloadWindowFullyVisibility();
        }

        private void ReloadWindowFullyVisibility()
        {
            foreach (var item in this.Controls.Cast<Control>())
            {
                if (item.Name == nameof(HideShowWindowButton))
                    item.Text = IsWindowFullyVisible ? ">" : "<";
                else
                    item.Visible = IsWindowFullyVisible;
            }

            if (IsWindowFullyVisible)
                SetButtonsVisibilityAccordingToWindowMode();

            SetWindowLocationAndSize();
        }

        private void SetWindowLocationAndSize()
        {
            if (IsWindowFullyVisible)
            {
                HideShowWindowButton.Location = new Point(144, 1);
                Size = new Size(170, 64);
            }
            else
            {
                HideShowWindowButton.Location = new Point(1, 1);
                Size = new Size(28, 28);
            }

            Rectangle workingArea = Screen.PrimaryScreen.WorkingArea;
            this.Left = workingArea.Left + workingArea.Width - this.Size.Width;
            this.Top = workingArea.Top + workingArea.Height - this.Size.Height - 100;
        }

        private void StartButton_Click(object sender, EventArgs e)
        {
            PomodoroAppContext.Start();
        }

        private void StopResumeButton_Click(object sender, EventArgs e)
        {
            PomodoroAppContext.StopResume();
        }

        private void ResetButton_Click(object sender, EventArgs e)
        {
            PomodoroAppContext.ResetClicked();
        }

        public void UpdateTime()
        {
            if (TimeLeftLabel.InvokeRequired)
            {
                Action safeWrite = delegate { TimeLeftLabel.Text = PomodoroAppContext.TimeLeft_UserFriendly; };
                TimeLeftLabel.Invoke(safeWrite);
            }
            else
            {
                TimeLeftLabel.Text = PomodoroAppContext.TimeLeft_UserFriendly;
            }        
        }
    }
}
