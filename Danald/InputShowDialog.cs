﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Danald
{
    public partial class InputShowDialog : Form
    {
        public InputShowDialog()
        {
            InitializeComponent();
            textBox1.Focus();
        }

        public string WorkDescription { get; internal set; }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            WorkDescription = textBox1.Text;
            this.Close();
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return &&
                (e.Shift || e.Control))
            {
                e.Handled = true;
                WorkDescription = textBox1.Text;
                this.Close();
            }

        }
    }
}
