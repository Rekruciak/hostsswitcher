﻿using Danald.AppContext;
using Danald.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Danald
{
    public class TrayAppContext : ApplicationContext
    {
        private const string BasePath = @"C:\Windows\System32\drivers\etc";
        private string _hostsFileFullPath = Path.Combine(BasePath, "hosts");

        private PomodoroAppContext PomodoroAppContext = new PomodoroAppContext();

        private NotifyIcon _trayIcon;
        public TrayAppContext()
        {
            _trayIcon = new NotifyIcon()
            {
                Icon = Resources.danald_5vz_icon,
                ContextMenu = new ContextMenu(new MenuItem[] {
                    new MenuItem("Pomodoro", PomodoroAppContext.Pomodoro_OnClick),
                    new MenuItem("Block", Block_OnClick),
                    new MenuItem("Unblock", new MenuItem[]{
                        new MenuItem("5 minutes", UnblockFor5Minutes_OnClick),
                        new MenuItem("15 minutes", UnblockFor15Minutes_OnClick),
                        new MenuItem("1 hour", UnblockForOneHour_OnClick),
                        //new MenuItem("Forever", UnblockForever_OnClick)
                    }),
                    new MenuItem("Edit config", EditConfig_OnClick),
                    new MenuItem("Restart (Reload config)", Restart_OnClick),
                    new MenuItem("Close", Close_OnClick),
                }),
                Visible = true
            };

            PomodoroAppContext.Pomodoro_OnClick(this, EventArgs.Empty);
        }

        private void UnblockFor5Minutes_OnClick(object sender, EventArgs e)
        {
            File.WriteAllText(_hostsFileFullPath,
                $@"
");
            LaunchMethodAfterCertainTime.Launch(60 * 5, Block);
        }

        private void UnblockForever_OnClick(object sender, EventArgs e)
        {
            File.WriteAllText(_hostsFileFullPath,
                $@"
");
        }

        private void UnblockForOneHour_OnClick(object sender, EventArgs e)
        {
            File.WriteAllText(_hostsFileFullPath,
                $@"
");
            LaunchMethodAfterCertainTime.Launch(60 * 60, Block);
        }

        private void UnblockFor15Minutes_OnClick(object sender, EventArgs e)
        {
            File.WriteAllText(_hostsFileFullPath,
                $@"
");
            LaunchMethodAfterCertainTime.Launch(60 * 15, Block);
        }

        private void Restart_OnClick(object sender, EventArgs e)
        {
            Process.Start(Path.Combine(Application.StartupPath, AppDomain.CurrentDomain.FriendlyName));
            Application.Exit();
        }

        private void EditConfig_OnClick(object sender, EventArgs e)
        {
            Process.Start("notepad.exe", Path.Combine(Environment.CurrentDirectory, AppDomain.CurrentDomain.FriendlyName + ".config"));
        }

        private void Close_OnClick(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private string GenerateBlockedHostsFileContent()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var site in ConfigurationManager.AppSettings.Get("BlockedSites").Split(';'))
                foreach (var suffix in ConfigurationManager.AppSettings.Get("BlockedSuffixes").Split(';'))
                {
                    sb.AppendLine($"127.0.0.1 www.{site}.{suffix}");
                    sb.AppendLine($"127.0.0.1 {site}.{suffix}");
                }
            return sb.ToString();
        }

        private void Block()
        {
            File.WriteAllText(_hostsFileFullPath,
                GenerateBlockedHostsFileContent());
            RestartBrowsersService.Restart();
        }

        private void Block_OnClick(object sender, EventArgs e)
        {
            Block();
        }
    }
}
