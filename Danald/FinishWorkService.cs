﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using static Danald.AppContext.PomodoroAppContext;

namespace Danald
{
    public class FinishWorkService
    {
        public class PomodoroWindowModeChangesLog
        {
            public WindowMode WindowMode;

            public DateTime ChangeTime = DateTime.Now;
        }

        public List<PomodoroWindowModeChangesLog> Logs = new List<PomodoroWindowModeChangesLog>();

        private static FinishWorkService _instance;
        public static FinishWorkService Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new FinishWorkService();
                return _instance;
            }
        }

        private string PomodoroDirectoryMain => Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "PomodoroTracker");
        private string PomodoroDirectoryCurrent => Path.Combine(PomodoroDirectoryMain, DateTime.Now.ToString("yyyy-MM"));
        private string CurrentFilePath => Path.Combine(PomodoroDirectoryCurrent, $"{DateTime.Now.ToString("dd dddd")}.txt");

        public FinishWorkService()
        {
            if (!Directory.Exists(PomodoroDirectoryMain))
                Directory.CreateDirectory(PomodoroDirectoryMain);
            if (!Directory.Exists(PomodoroDirectoryCurrent))
                Directory.CreateDirectory(PomodoroDirectoryCurrent);
        }

        public string GetWorkDescription()
        {
            InputShowDialog inputShowDialog = new InputShowDialog();
            inputShowDialog.ShowDialog();
            return inputShowDialog.WorkDescription;
        }

        public void LogPomodoroWindowModeChanged(WindowMode windowMode)
        {
            Logs.Add(new PomodoroWindowModeChangesLog { WindowMode = windowMode });
        }

        public void SaveFinishedWork()
        {
            var workDescription = GetWorkDescription();

            int workTimeInSeconds = 0;
            foreach (var workingMode in Logs.Where(q => q.WindowMode == WindowMode.Working))
            {
                workTimeInSeconds += (int)(Logs[Logs.IndexOf(workingMode) + 1].ChangeTime - workingMode.ChangeTime).TotalSeconds;
            }

            StringBuilder fileAppendContent = new StringBuilder();
            fileAppendContent.AppendLine($"~~~~ {workDescription} ({workTimeInSeconds / 60} min) ~~~~");
            foreach (var log in Logs)
            {
                if (log.WindowMode == WindowMode.ReadyToStart)
                    continue;
                fileAppendContent.AppendLine($"{log.ChangeTime:HH:mm:ss} {log.WindowMode}");
            }
            fileAppendContent.AppendLine();
            fileAppendContent.AppendLine();

            File.AppendAllText(CurrentFilePath, fileAppendContent.ToString());

            Logs.Clear();
        }

        public void Reset()
        {
            int workTimeInSeconds = 0;
            foreach (var workingMode in Logs.Where(q => q.WindowMode == WindowMode.Working))
            {
                workTimeInSeconds += Logs.IndexOf(workingMode) + 1 < Logs.Count 
                    ? (int)(Logs[Logs.IndexOf(workingMode) + 1].ChangeTime - workingMode.ChangeTime).TotalSeconds
                    : (int)(DateTime.Now - workingMode.ChangeTime).TotalSeconds;
            }

            StringBuilder fileAppendContent = new StringBuilder();
            fileAppendContent.AppendLine($"~~~~ RESET ({workTimeInSeconds / 60} min) ~~~~");
            foreach (var log in Logs)
                fileAppendContent.AppendLine($"{log.ChangeTime:HH:mm:ss} {log.WindowMode}");
            fileAppendContent.AppendLine();
            fileAppendContent.AppendLine();

            File.AppendAllText(CurrentFilePath, fileAppendContent.ToString());

            Logs.Clear();
        }
    }
}
