﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Danald
{
    public class LaunchMethodAfterCertainTime
    {
        static BackgroundWorker bw;
        public static void Launch(int timeInSecounds, Action action)
        {
            if (bw != null)
                bw.CancelAsync();

            bw = new BackgroundWorker()
            {
                WorkerSupportsCancellation = true
            };
            bw.DoWork += ((sender, e) =>
            {
                Thread.Sleep(timeInSecounds * 1000);
                if (!(sender as BackgroundWorker).CancellationPending)
                    action();
            });
            bw.RunWorkerAsync();
        }
    }
}
